import * as fs from 'node:fs/promises'
import * as pt from 'node:path'

const root = pt.join(__dirname, '..')

const main = async () => {
	// esbuild が esm 対応してなさそう
	await fs.copyFile(
		pt.join(root, 'node_modules/chevrotain/lib/chevrotain.mjs'),
		pt.join(root, 'src/chevrotain/impl.ts'),
	)
}

main().catch(x => {
	console.error(x)
	process.exit(1)
})
