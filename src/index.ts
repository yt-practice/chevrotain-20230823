import { inspect } from 'node:util'

import { hoge } from './hoge'
import { parse } from './parser'

const log = (value: unknown) => {
	console.log(inspect(value, false, 25, true))
}

const main = () => {
	log(parse('1 + 2'))
	log(parse('"1" + 2'))
	log(parse('5 - 2'))
	log(parse('1 - "2"'))
	log(parse('"1 + 2"'))
	log(parse('hoge + 2'))
	log(parse('fuga+2'))
	log(hoge())
}

main()
