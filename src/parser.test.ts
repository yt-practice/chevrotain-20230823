import { parse } from './parser'

describe('parse', () => {
	type Result = ReturnType<typeof parse>

	const list: {
		str: string
		ret: Result['cst']
	}[] = [
		{
			str: 'hoge',
			ret: { is: 'ident', ident: 'hoge' },
		},
		{
			str: '1 + 2',
			ret: {
				is: 'op:+',
				left: { is: 'int', int: 1 },
				right: { is: 'int', int: 2 },
			},
		},
		{
			str: '"1" + 2',
			ret: {
				is: 'op:+',
				left: { is: 'string', str: '"1"' },
				right: { is: 'int', int: 2 },
			},
		},
		{
			str: '5 - 2',
			ret: {
				is: 'op:-',
				left: { is: 'int', int: 5 },
				right: { is: 'int', int: 2 },
			},
		},
		{
			str: '1 - "2"',
			ret: {
				is: 'op:-',
				left: { is: 'int', int: 1 },
				right: { is: 'string', str: '"2"' },
			},
		},
		{
			str: '"1 + 2"',
			ret: { is: 'string', str: '"1 + 2"' },
		},
		{
			str: 'hoge + 2',
			ret: {
				is: 'op:+',
				left: { is: 'ident', ident: 'hoge' },
				right: { is: 'int', int: 2 },
			},
		},
		{
			str: 'fuga+2',
			ret: {
				is: 'op:+',
				left: { is: 'ident', ident: 'fuga' },
				right: { is: 'int', int: 2 },
			},
		},
		{
			str: '-1 + -2',
			ret: {
				is: 'op:+',
				left: { is: 'int', int: -1 },
				right: { is: 'int', int: -2 },
			},
		},
		{
			str: '-1  -2',
			ret: {
				is: 'op:-',
				left: { is: 'int', int: -1 },
				right: { is: 'int', int: 2 },
			},
		},
		{
			str: '-1-2',
			ret: {
				is: 'op:-',
				left: { is: 'int', int: -1 },
				right: { is: 'int', int: 2 },
			},
		},
		{
			str: 'amoda',
			ret: { is: 'ident', ident: 'amoda' },
		},
		{
			str: 'hoge mod fuga',
			ret: {
				is: 'op:%',
				left: { is: 'ident', ident: 'hoge' },
				right: { is: 'ident', ident: 'fuga' },
			},
		},
	]
	for (const { str, ret: cst } of list)
		it(JSON.stringify(str), () => {
			expect(parse(str)).toEqual<Result>({
				str,
				cst,
				lexErrors: [],
				parseErrors: [],
			})
		})
})
