// import { createToken, CstParser, Lexer } from 'chevrotain'
import { createToken, EmbeddedActionsParser, Lexer } from './chevrotain'

const String = createToken({ name: 'String', pattern: /"[^"]*"|\{[^}]*\}/ })
const Operator = createToken({ name: 'Operator', pattern: /mod|\*\*|[*/]/ })
const Int = createToken({ name: 'Int', pattern: /0|[1-9]\d*/ })
const Sign = createToken({ name: 'Sign', pattern: /[+-]/ })
const Ident = createToken({ name: 'Ident', pattern: /[^\s+\-*/]+/ })

const Whitespace = createToken({
	name: 'Whitespace',
	pattern: /\s+/,
	group: Lexer.SKIPPED,
})

const tokens = [Whitespace, String, Int, Sign, Operator, Ident]

const createIndexStore = () => {
	let inc = 0
	const map: Partial<Record<string, number>> = {}
	return (key: string): number => (map[key] ??= inc++)
}

type Value = StringValue | IntValue | IdentValue
type StringValue = Readonly<{ is: 'string'; str: string }>
type IntValue = Readonly<{ is: 'int'; int: number }>
type IdentValue = Readonly<{ is: 'ident'; ident: string }>
type Op = 'op:+' | 'op:-' | 'op:*' | 'op:/' | 'op:%' | 'op:^'
type OpFormula = Readonly<{ is: Op; left: Value; right: Value }>
type Formula = OpFormula | Value

class FormulaParser extends EmbeddedActionsParser {
	public constructor() {
		super(tokens)
		this.performSelfAnalysis()
	}
	private readonly key = createIndexStore()
	private readonly value = this.RULE('value', () =>
		this.OR<Value>([
			{
				ALT: () => {
					const { image: str } = this.consume(this.key('value'), String)
					return { is: 'string', str } as const
				},
			},
			{
				ALT: () => {
					const s = this.option(this.key('value'), () =>
						this.consume(this.key('value'), Sign),
					)
					const n = '-' === s?.image ? -1 : 1
					const { image: int } = this.consume(this.key('value'), Int)
					return { is: 'int', int: n * Number(int) } as const
				},
			},
			{
				ALT: () => {
					const { image: ident } = this.consume(this.key('value'), Ident)
					return { is: 'ident', ident } as const
				},
			},
		]),
	)
	private readonly opFormula = this.RULE('opFormula', () => {
		const left = this.subrule(this.key('opFormula1'), this.value)
		const { image: op } = this.OR([
			{ ALT: () => this.consume(this.key('opFormula2'), Sign) },
			{ ALT: () => this.consume(this.key('opFormula2'), Operator) },
		])
		// this.consume(this.key('opFormula2'), Operator)
		const is = (() => {
			switch (op) {
				case '+':
					return 'op:+'
				case '-':
					return 'op:-'
				case '*':
					return 'op:*'
				case '/':
					return 'op:/'
				case 'mod':
					return 'op:%'
				case '**':
					return 'op:^'
				default:
					return null as never
			}
		})()
		const right = this.subrule(this.key('opFormula3'), this.value)
		return { is, left, right } as const
	})
	public formula = this.RULE('formula', () =>
		this.OR<Formula>([
			{ ALT: () => this.subrule(this.key('formula'), this.opFormula) },
			{ ALT: () => this.subrule(this.key('formula'), this.value) },
		]),
	)
}

const lexer = new Lexer(tokens, { positionTracking: 'onlyOffset' })
const parser = new FormulaParser()

export const parse = (str: string) => {
	const lexResult = lexer.tokenize(str)
	parser.input = lexResult.tokens
	const cst = parser.formula()
	return {
		str,
		// This is a pure grammar, the value will be undefined until we add embedded actions
		// or enable automatic CST creation.
		cst,
		// tokens: lexResult.tokens,
		lexErrors: lexResult.errors,
		parseErrors: parser.errors,
	} as const
}
